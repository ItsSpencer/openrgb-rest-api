import * as express from 'express';
import { Client as OpenRGBClient} from 'openrgb-sdk';
import Client from 'openrgb-sdk/types/client.js';
import Device from 'openrgb-sdk/types/device.js';

async function main() {
    // Connect with sdk
    let client: Client = new OpenRGBClient('Neighbourhood Life - Integration for OpenRGB', 6742, 'localhost');
    await client.connect();

    const controllers = await client.getAllControllerData();
    const keyboards: Device[] = [];

    for (let i=0; i<controllers.length; i++) {
        const controller = await controllers[i];
        if (controller.type === 5) {
            keyboards.push(controller);
        }
    }

    console.log(`[OpenRGB API] Found ${keyboards.length} keyboards.`);

    // Provide rest api
    const app: express.Express = express.default();
    const port = 12345;
    
    app.use(express.json());

    app.get("/", (req: express.Request, res: express.Response) => {
        res.send("Hello world!");
    })

    // E.g. ledname: Key: Number Pad 1
    /*
        {
            "color": {
                "r": 0,
                "g": 255,
                "b": 0
            },
            "ledname": "Key: Number Pad 1"
        }
    */
    app.put("/updateled", (req: express.Request, res: express.Response) => {
        const color = req.body.color;
        const ledname = req.body.ledname;

        console.log(`[OpenRGB API] Update ${ledname ? ledname : "all leds"} to color ${color}.`);

        if (ledname) {
            keyboards.forEach((device) => {
                const ledId = device.leds.findIndex((led) => {return led.name === ledname});
    
                if (ledId >= 0) {
                    client.updateSingleLed(device.deviceId,ledId,{
                        red: color.r,
                        green: color.g,
                        blue: color.b
                    });
                }
            })
        } else {
            keyboards.forEach((device) => {
                var colors = Array(device.colors.length).fill({
                    red: color.r,
                    green: color.g,
                    blue: color.b
                });
                
                client.updateLeds(device.deviceId, colors);
            })
        }

        res.send("Done!");
    })

    app.listen(port, () => {
        console.log(`[OpenRGB API] Server is running at http://localhost:${port}.`);
    });
}

main();