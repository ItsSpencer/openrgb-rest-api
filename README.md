# OpenRGB HTTP Server


## Commands

- Test: Run "npm run start"
- Build: Run "npm run build"


## Usage

Command | URL | Info
------------ | ------------- | -------------
 | http://localhost:12345/ | Receive a warming welcome from the API.
Update LED | http://localhost:8080/updateled | PUT-Request to update one specific led or all leds.

## Example for json body

{
  "color": {
      "r": 0,
      "g": 255,
      "b": 0
  },
  "ledname": "Key: Number Pad 1"
}

## Notes
Many thanks to the following:
- vlakreeh for the NodeJS OpenRGB Client (https://www.npmjs.com/~vlakreeh)
- bencevans for the colour fading library (https://www.npmjs.com/~bencevans)
- mola19 for the NodeJS OpenRGB-SDK Client (https://www.npmjs.com/package/openrgb-sdk)

And also credits to:
- Original Author: https://github.com/RelishedChicken
- Original Repository: https://github.com/RelishedChicken/OpenRGBHTTPServer
- OpenRGB SDK Documentation: https://gitlab.com/OpenRGBDevelopers/OpenRGB-Wiki/-/blob/stable/Developer-Documentation/OpenRGB-SDK-Documentation.md
